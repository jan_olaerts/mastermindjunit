package be.janolaerts.mastermindjunit;

import java.util.Scanner;

public class ConsoleTool {

    private final Scanner scanner = new Scanner(System.in);

    public String askSpecificAmountOfPosDigitsStringBetweenRange(String message, int length, int lowDigit, int highDigit) {
        if (length < 1 || highDigit < lowDigit || lowDigit < 0 || lowDigit > 9 || highDigit > 9) {
            return null;
        }

        String input;

        do {
            System.out.println(message);
            input = scanner.nextLine();
            if (!input.matches("[" + lowDigit + "-" + highDigit + "]{" + length + "}")) {
                System.err.println("Type in exactly " + length + " digits between " + lowDigit + " and " + highDigit);
            }
        } while (!input.matches("[" + lowDigit + "-" + highDigit + "]{" + length + "}"));

        return input;
    }

    public boolean askYesOrNo(String message) {
        if(message == null || message.length() < 1) return false;
        System.out.println(message);
        String answer;
        do {
            answer = scanner.nextLine().toLowerCase();
            if (answer.length() > 1) System.err.println("Answer cannot be longer than 1 char");
            if (!answer.equals("y") && !answer.equals("n")) System.err.println("Type in y or n");
        } while (!answer.equals("y") && !answer.equals("n"));

        return answer.equals("y");
    }

    public boolean checkNoDoublesInString(String input) {
        if (input == null || input.length() < 1) return false;

        for(int i = 0; i < input.length(); i++) {
            for(int j = 0; j < input.length(); j++) {
                if (i == j) continue;

                if(input.charAt(i) == input.charAt(j)) {
                    System.err.println("The input contains doubles");
                    return false;
                }
            }
        }

        return true;
    }
}