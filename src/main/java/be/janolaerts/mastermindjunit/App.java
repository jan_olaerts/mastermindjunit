package be.janolaerts.mastermindjunit;

public class App {

    private static final ConsoleTool consoleInput = new ConsoleTool();

    public static void main( String[] args ) {
        App app = new App();
        app.startGame();
        app.runGame(new Mastermind());
    }

    public void startGame() {
        System.out.println("Welcome to Mastermind!");
        System.out.println("I'm thinking of a 4 digit code, with numbers starting from 1 up to 6.");
        System.out.println("Duplicate values are not allowed.");
        System.out.println(" '+' represents a correct number guessed in the correct position");
        System.out.println(" '-' represents a correct number guessed, but in the wrong position.");
    }

    public void runGame(Mastermind mastermind) {
        if (mastermind == null) {
            throw new RuntimeException("mastermind cannot be null and must be an object of Mastermind");
        }

        System.out.println("The answer is: " + mastermind.getAnswer());
        String result;

        do {
            // Increment guesses by 1
            mastermind.setGuesses(mastermind.getGuesses() + 1);

            String guess;
            do {
                guess = consoleInput.askSpecificAmountOfPosDigitsStringBetweenRange(
                                "Guess " + mastermind.getGuesses() + ":",
                                mastermind.getAnswer().length(), 1, 6);
            } while (!consoleInput.checkNoDoublesInString(guess));

            // Check guess
            result = mastermind.checkGuess(guess);
            if (!result.equals(Mastermind.CORRECT_GUESS)) {
                System.out.println(result);
            }

        } while (!result.equals(Mastermind.CORRECT_GUESS));
        winGame(mastermind);
    }

    public void winGame(Mastermind mastermind) {
        if (mastermind == null) {
            throw new RuntimeException("mastermind cannot be null and must be an object of Mastermind");
        }

        System.out.println("You guessed the correct answer!");
        System.out.println("You guessed " + mastermind.getGuesses() + " times in this round");
        Mastermind.timesPlayed++;
        mastermind.setFinishedGame(true);

        if(consoleInput.askYesOrNo("Do you want to play again (y/n)")) {
            loopGame(mastermind);
        } else {
            System.out.println("Thank you for playing!");
            System.out.println("You played " + Mastermind.timesPlayed + " times");
            mastermind.setFinishedGame(true);
        }
    }

    public void loopGame(Mastermind mastermind) {
        if (mastermind == null) {
            throw new RuntimeException("mastermind cannot be null and must be an object of Mastermind");
        }

        mastermind.reset();
        startGame();
        runGame(mastermind);
    }
}