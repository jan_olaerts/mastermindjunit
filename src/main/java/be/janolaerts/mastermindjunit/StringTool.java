package be.janolaerts.mastermindjunit;

public class StringTool {

    public static String setX(String input, int index) {
        if (input == null
                || index < 0
                || index > input.length() -1
        ) return input;

        StringBuilder answer = new StringBuilder(input);
        answer.replace(index, index+1, "X");
        return answer.toString();
    }
}