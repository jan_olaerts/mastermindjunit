package be.janolaerts.mastermindjunit;

public class AnswerCanOnlyContainDigitsOneToSixException extends IllegalArgumentException {

    public AnswerCanOnlyContainDigitsOneToSixException() {
        super("Input can only contain digits from 1 to 6");
    }
}