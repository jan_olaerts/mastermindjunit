package be.janolaerts.mastermindjunit;

import java.util.Random;

public class Mastermind {
    private static final Random rand = new Random();

    public static final String FULL_VALID_GUESS = "+";
    public static final String SEMI_VALID_GUESS = "-";
    public static final String CORRECT_GUESS = "++++";
    public static int timesPlayed = 0;

    private int guesses;
    private String answer;
    private boolean finishedGame;

    public Mastermind() {
        createRandomAnswer();
        setGuesses(0);
        setFinishedGame(false);
    }

    public String getAnswer() {
        return this.answer;
    }

    public void setAnswer(String answer) {
        if (answer == null) throw new AnswerCannotBeNullException();
        if (answer.length() != 4) throw new AnswerLengthMustBeFourException();
        if (!answer.matches("[1-6]{4}")) throw new AnswerCanOnlyContainDigitsOneToSixException();

        this.answer = answer;
    }

    public int getGuesses() {
        return this.guesses;
    }

    public void setGuesses(int guesses) {
        if (guesses < 0) throw new IllegalArgumentException("Guesses cannot be less than 0");
        this.guesses = guesses;
    }

    public void createRandomAnswer() {
        StringBuilder sb = new StringBuilder();
        int count = 0;

        do {
            int num = rand.nextInt(6) + 1;
            if (!sb.toString().contains(Integer.toString(num))) {
                sb.append(num);
                count++;
            }
        } while (count < 4);

        setAnswer(sb.toString());
    }

    public String checkGuess(String guess) {
        if (guess.length() < getAnswer().length()
                || guess.length() > getAnswer().length()
                || guess.length() == 0
                || !guess.matches("[1-6]{4}")) {
            throw new IllegalArgumentException("Guess must contain 4 digits between 1 and 6");
        }
        String answer = this.getAnswer();
        StringBuilder correctPos = new StringBuilder();
        StringBuilder notCorrectPos = new StringBuilder();

        for(int i = 0; i < answer.length(); i++) {

            // Check match on the same position
            if(answer.charAt(i) == guess.charAt(i)) {
                correctPos.append(FULL_VALID_GUESS);
                answer = StringTool.setX(answer, i);
                continue;
            }

            // Check matches for all positions
            for (int j = 0; j < guess.length(); j++) {
                if (i == j) continue;

                if (answer.charAt(i) == guess.charAt(j)) {
                    notCorrectPos.append(SEMI_VALID_GUESS);
                    answer = StringTool.setX(answer, i);
                    break; // Breaking out of guess loop since answer is all unique chars
                }
            }
        }

        return correctPos.toString() + notCorrectPos.toString();
    }

    public boolean isFinishedGame() {
        return this.finishedGame;
    }

    public void setFinishedGame(boolean finishedGame) {
        this.finishedGame = finishedGame;
    }

    public void reset() {
        setGuesses(0);
        createRandomAnswer();
        setFinishedGame(false);
    }
}