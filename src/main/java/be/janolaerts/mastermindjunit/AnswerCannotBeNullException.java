package be.janolaerts.mastermindjunit;

public class AnswerCannotBeNullException extends RuntimeException {

    public AnswerCannotBeNullException() {
        super("Input cannot be null");
    }
}