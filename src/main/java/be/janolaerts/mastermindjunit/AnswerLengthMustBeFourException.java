package be.janolaerts.mastermindjunit;

public class AnswerLengthMustBeFourException extends IllegalArgumentException {

    public AnswerLengthMustBeFourException() {
        super("Input length must be exactly four chars");
    }
}