package be.janolaerts.mastermindjunit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AppTest {

    private App app;
    private Mastermind mastermind;

    @BeforeEach
    protected void init() {
        app = new App();
        mastermind = new Mastermind();
    }

    @Test
    protected void testLoopGame() {
        assertThrows(RuntimeException.class, () -> app.loopGame(null));
        assertEquals(0, mastermind.getGuesses());
        assertFalse(mastermind.isFinishedGame());
    }
}