package be.janolaerts.mastermindjunit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class MastermindTest {

    private Mastermind mastermind;

    @BeforeEach
    protected void init() {
        mastermind = new Mastermind();
        mastermind.setAnswer("1234");
    }

    @Test
    protected void testGetAnswer() {
        assertEquals("1234", mastermind.getAnswer());
    }

    @Test
    protected void testSetAnswer() {

        assertAll(
                () -> assertEquals("1234", mastermind.getAnswer()),
                () -> assertThrows(AnswerCanOnlyContainDigitsOneToSixException.class, () -> mastermind.setAnswer("1823")),
                () -> assertThrows(AnswerCanOnlyContainDigitsOneToSixException.class, () -> mastermind.setAnswer("1230")),
                () -> assertThrows(AnswerCanOnlyContainDigitsOneToSixException.class, () -> mastermind.setAnswer("123k")),
                () -> assertThrows(AnswerLengthMustBeFourException.class, () -> mastermind.setAnswer("123")),
                () -> assertThrows(AnswerLengthMustBeFourException.class, () -> mastermind.setAnswer("12345")),
                () -> assertThrows(AnswerCannotBeNullException.class, () -> mastermind.setAnswer(null))
        );
    }

    @Test
    protected void testGetGuesses() {
        mastermind.setGuesses(3);

        assertAll(
                () -> assertEquals(3, mastermind.getGuesses()),
                () -> assertThrows(IllegalArgumentException.class, () -> mastermind.setGuesses(-1))
        );
    }

    @Test
    protected void testSetGuesses() {
        mastermind.setGuesses(6);

        assertAll(
                () -> assertEquals(6, mastermind.getGuesses()),
                () -> assertThrows(IllegalArgumentException.class, () -> mastermind.setGuesses(-1))
        );
    }

    @Test
    protected void testCheckGuessValidation() {
        mastermind.setAnswer("6541");

        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> mastermind.checkGuess("123")),
                () -> assertThrows(IllegalArgumentException.class, () -> mastermind.checkGuess("12345")),
                () -> assertThrows(IllegalArgumentException.class, () -> mastermind.checkGuess("")),
                () -> assertThrows(IllegalArgumentException.class, () -> mastermind.checkGuess("1230")),
                () -> assertThrows(IllegalArgumentException.class, () -> mastermind.checkGuess("1238")),
                () -> assertThrows(IllegalArgumentException.class, () -> mastermind.checkGuess("123k")),
                () -> assertThrows(IllegalArgumentException.class, () -> mastermind.checkGuess("123-")),
                () -> assertEquals(Mastermind.CORRECT_GUESS, mastermind.checkGuess("6541"))
        );
    }

    @ParameterizedTest(name = "Input -> {0}, Output -> {1}, Answer -> {2}")
    @CsvFileSource(resources = "/guesses.csv", numLinesToSkip = 1)
    protected void testCheckGuessValues(String input, String output, String answer) {
        mastermind.setAnswer(answer);
        assertEquals(output, mastermind.checkGuess(input));
    }

    @Test
    protected void testIsFinishedGame() {
        mastermind.setFinishedGame(true);
        assertTrue(mastermind.isFinishedGame());

        mastermind.setFinishedGame(false);
        assertFalse(mastermind.isFinishedGame());
    }

    @Test
    protected void testSetFinishedGame() {
        mastermind.setFinishedGame(true);
        assertTrue(mastermind.isFinishedGame());

        mastermind.setFinishedGame(false);
        assertFalse(mastermind.isFinishedGame());
    }

    @Test
    protected void testReset() {
        mastermind.reset();
        assertAll(
                () -> assertEquals(0, mastermind.getGuesses()),
                () -> assertTrue(mastermind.getAnswer().matches("[1-6]{4}")),
                () -> assertFalse(mastermind.isFinishedGame())
        );
    }
}